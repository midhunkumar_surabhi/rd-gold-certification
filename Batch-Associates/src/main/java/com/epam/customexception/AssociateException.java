package com.epam.customexception;

public class AssociateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5097274685580001475L;
	
	public AssociateException(String message) {
		super(message);
	}

}
