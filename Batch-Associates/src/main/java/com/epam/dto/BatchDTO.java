package com.epam.dto;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class BatchDTO {
	
	private int id;
	
	@NotBlank(message = "Name should not be blank")
	@Size(min = 2, message = "Name should have atleast 2 characters")
	private String name;
	
	@NotBlank(message = "practice should not be blank")
	@Size(min = 2, message = "practice should have atleast 2 characters")
	private String practice;
	
	@NotBlank(message = "Start Date should not be blank")
	private Date startDate;
	
	@NotBlank(message = "End Date should not be blank")
	private Date endDate;

}
