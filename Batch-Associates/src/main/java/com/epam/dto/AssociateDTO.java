package com.epam.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssociateDTO {
	
	private int id;
	
	@NotBlank(message = "Name should not be blank")
	@Size(min = 3, message = "Name should have atleast 3 characters")
	private String name;
	
	@Email(message = "Email should be in proper format")
	private String email;
	
	@NotBlank(message = "Gender should not be blank")
	@Size(max = 1, message = "Gender should have only one character")
	private String gender;
	
	@NotBlank(message = "college name should not be blank")
	private String college;
	
	@NotBlank(message = "status should not be blank")
	private String staus;
	
	@NotNull(message = "Batch should not be null")
	private BatchDTO batch;

}
