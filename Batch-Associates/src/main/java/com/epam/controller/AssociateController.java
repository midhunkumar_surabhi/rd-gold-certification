package com.epam.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDTO;
import com.epam.service.AssociateService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rd/associates")
@RequiredArgsConstructor
@Slf4j
public class AssociateController {
	
	private final AssociateService associateService;
	
	@PostMapping
	public ResponseEntity<AssociateDTO> addAssociate(@RequestBody @Valid AssociateDTO associateDTO){
		log.info("Entered Add Associate controller, Associate is {}",associateDTO);
		return new ResponseEntity<>(associateService.addAssociate(associateDTO),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removeAssociate(@PathVariable int id){
		log.info("Entered remove Associate controller, with id {}",id);
		associateService.removeAssociate(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping
	public ResponseEntity<AssociateDTO> updateAssociate(@RequestBody @Valid AssociateDTO associateDTO){
		log.info("Entered update Associate controller, Associate is {}",associateDTO);
		return new ResponseEntity<>(associateService.updateAssocaiate(associateDTO),HttpStatus.OK);
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDTO>> findAllAssociatesByGender(@PathVariable String gender){
		log.info("Entered get Associate by gender controller, gender is {}",gender);
		return new ResponseEntity<>(associateService.getAssociatesByGender(gender),HttpStatus.OK);
	}
	

}
