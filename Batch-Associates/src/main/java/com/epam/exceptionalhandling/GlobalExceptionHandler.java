package com.epam.exceptionalhandling;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.customexception.AssociateException;
import com.epam.dto.ErrorResponse;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(AssociateException.class)
	public ResponseEntity<ErrorResponse> assocaiateExceptionHandling(AssociateException associateException, WebRequest request){
		log.error(ExceptionUtils.getStackTrace(associateException));
		return new ResponseEntity<>(new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),associateException.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> methodArgumentExceptionHandling(MethodArgumentNotValidException methodArgumentNotValidException, WebRequest request){
		Map<String,String> result = new HashMap<>();
		methodArgumentNotValidException.getBindingResult().getFieldErrors().forEach(error -> result.put(error.getField(),error.getDefaultMessage()));
		log.error(ExceptionUtils.getStackTrace(methodArgumentNotValidException));
		return new ResponseEntity<>(new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), result.toString(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ErrorResponse> runtimeExceptionHandling(RuntimeException runtimeException, WebRequest request){
		log.error(ExceptionUtils.getStackTrace(runtimeException));
		return new ResponseEntity<>(new ErrorResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),runtimeException.getMessage(), request.getDescription(false)), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ErrorResponse> sqlExceptionHandling(DataIntegrityViolationException dataIntegrityViolationException, WebRequest request){
		String message = "Entered input is not Valid";
		log.error(ExceptionUtils.getStackTrace(dataIntegrityViolationException));
		return new ResponseEntity<>(new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),message, request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}
	
}
