package com.epam.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.customexception.AssociateException;
import com.epam.dto.AssociateDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class AssociateServiceImpl implements AssociateService {
	
	private final AssociateRepository associateRepository;
	private final BatchRepository batchRepository;
	private final ModelMapper modelMapper;

	@Override
	public AssociateDTO addAssociate(AssociateDTO associateDTO) {
		
		log.info("Entered Add Associate, Associate is {}",associateDTO);
		
		Associate associate = modelMapper.map(associateDTO, Associate.class);
		Batch batch = associate.getBatch();
		if(!batchRepository.existsById(batch.getId())) {
			batchRepository.save(batch);
		}
		associate.setBatch(batch);
		
		return modelMapper.map(associateRepository.save(associate),AssociateDTO.class);
		
	}

	@Override
	public void removeAssociate(int id) {
		log.info("Entered remove Associate, with id {}",id);
		associateRepository.deleteById(id);
	}

	@Override
	public AssociateDTO updateAssocaiate(AssociateDTO associateDTO) {
		
		log.info("Entered update Associate, Associate is {}",associateDTO);
		
		if(!associateRepository.existsById(associateDTO.getId())) {
			
			log.info("Associate with id {} alrady exist",associateDTO.getId());
			throw new AssociateException(String.format("Associate with Id %d didn't exist", associateDTO.getId()));
		}
		
		Associate associate = modelMapper.map(associateDTO, Associate.class);
		Batch batch = associate.getBatch();
		
		if(!batchRepository.existsById(batch.getId())) {
			batchRepository.save(batch);
		}
		associate.setBatch(batch);
		
		return modelMapper.map(associateRepository.save(associate),AssociateDTO.class);
		
	}

	@Override
	public List<AssociateDTO> getAssociatesByGender(String gender) {
		log.info("Entered get Associate by gender, gender is {}",gender);
		return associateRepository.findAllByGender(gender)
				     .stream()
				     .map(associate -> modelMapper.map(associate, AssociateDTO.class))
				     .toList();
	}
	
	

}
