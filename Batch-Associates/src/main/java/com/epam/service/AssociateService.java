package com.epam.service;

import java.util.List;

import com.epam.dto.AssociateDTO;

public interface AssociateService {
	
	AssociateDTO addAssociate(AssociateDTO associateDTO);
	
	void removeAssociate(int id);
	
	AssociateDTO updateAssocaiate(AssociateDTO associateDTO);
	
	List<AssociateDTO> getAssociatesByGender (String gender);
	
}
