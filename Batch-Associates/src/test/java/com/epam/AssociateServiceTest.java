package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.customexception.AssociateException;
import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.impl.AssociateServiceImpl;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {
	
	private Associate associate;
	
	private AssociateDTO associateDTO;
	
	private Batch batch;
	
	private BatchDTO batchDTO;
	
	@Mock
	private BatchRepository batchRepository;
	
	@Mock
	private AssociateRepository associateRepository;
	
	@Mock
	private ModelMapper modelMapper;
	
	@InjectMocks
	private AssociateServiceImpl associateService;
	
	@Test
	void testAddAssociate() {
		Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(true);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDTO, associateService.addAssociate(associateDTO));
	}
	
	@Test
	void testAddAssociateWithNewBatch() {
		Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(false);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDTO, associateService.addAssociate(associateDTO));
	}
	
	@Test
	void testRemoveAssociate() {
		Mockito.doNothing().when(associateRepository).deleteById(1);
		associateService.removeAssociate(1);
		Mockito.verify(associateRepository).deleteById(1);
	}
	
	@Test
	void testUpdateAssociate() {
		Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(true);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(associateRepository.existsById(1)).thenReturn(true);
		assertEquals(associateDTO, associateService.updateAssocaiate(associateDTO));
	}
	
	@Test
	void testUpdateAssociateWithNewBatch() {
		Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.existsById(0)).thenReturn(false);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(associateRepository.existsById(1)).thenReturn(true);
		assertEquals(associateDTO, associateService.updateAssocaiate(associateDTO));
	}
	
	@Test
	void testUpdateAssociateWithException() {
		Mockito.when(associateRepository.existsById(1)).thenReturn(false);
		assertThrows(AssociateException.class, () -> associateService.updateAssocaiate(associateDTO));
	}
	
	@Test
	void testFetchByGender() {
		List<AssociateDTO> associatesDTOs = Arrays.asList(associateDTO);
		List<Associate> associates = Arrays.asList(associate);
		
		Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(associateRepository.findAllByGender("M")).thenReturn(associates);
		assertEquals(associatesDTOs, associateService.getAssociatesByGender("M"));
	}
	
	
	@BeforeEach
	void setUpAssociate() {
		associate = new Associate();
		associate.setCollege("GPREC");
		associate.setEmail("midhun@epam.com");
		associate.setGender("M");
		associate.setId(1);
		associate.setStaus("ACTIVE");
		associate.setName("Midhun");
		associate.setBatch(batch);
		
		associateDTO = AssociateDTO.builder().batch(batchDTO).college("GPREC")
				                          .email("midhun@epam.com")
				                          .gender("M")
				                          .id(1)
				                          .staus("ACTIVE")
				                          .name("Midhun").build();
		
	}
	
	@BeforeEach
	void setUpBatch() {
		batch = new Batch();
		batch.setAssociates(Arrays.asList(associate));
		batch.setEndDate(new Date(10));
		batch.setStartDate(new Date(9));
		batch.setPractice("Java");
		batch.setName("Raj");
		
		batchDTO = BatchDTO.builder().endDate(new Date(10))
				           .startDate(new Date(9))
				           .practice("Java")
				           .name("Raj")
				           .id(1)
				           .build();
		
	}


}
