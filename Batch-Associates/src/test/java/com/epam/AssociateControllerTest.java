package com.epam;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.customexception.AssociateException;
import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class AssociateControllerTest {
	
	private Associate associate;
	
	private AssociateDTO associateDTO;
	
	private Batch batch;
	
	private BatchDTO batchDTO;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private AssociateService associateService;
	
	
	@Test
	void addAssociate() throws JsonProcessingException, Exception {
		Mockito.when(associateService.addAssociate(any(AssociateDTO.class))).thenReturn(associateDTO);
		
		mockMvc.perform(post("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate)))
		        .andExpect(status().isCreated());
		
		Mockito.verify(associateService).addAssociate(any(AssociateDTO.class));
	}
	
	@Test
	void addAssociateWithDataIntegrityException() throws JsonProcessingException, Exception {
		Mockito.when(associateService.addAssociate(any(AssociateDTO.class))).thenThrow(DataIntegrityViolationException.class);
		
		mockMvc.perform(post("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate)))
		        .andExpect(status().isBadRequest());
		
		Mockito.verify(associateService).addAssociate(any(AssociateDTO.class));
	}
	
	@Test
	void addAssociateWithInvalidArguments() throws JsonProcessingException, Exception {
		associate.setName("h");
		
		mockMvc.perform(post("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate)))
		        .andExpect(status().isBadRequest());
		
	}
	
	@Test
	void deleteAssociate() throws JsonProcessingException, Exception {
		
		Mockito.doNothing().when(associateService).removeAssociate(1);
		
		mockMvc.perform(delete("/rd/associates/{id}",1))
		        .andExpect(status().isNoContent());
		
		Mockito.verify(associateService).removeAssociate(1);
		
	}
	
	@Test
	void deleteAssociateWithInvalidInput() throws JsonProcessingException, Exception {
		
		mockMvc.perform(delete("/rd/associates/{id}","a"))
		        .andExpect(status().isInternalServerError());
		
	}
	
	@Test
	void updateAssociate() throws JsonProcessingException, Exception {
		
		Mockito.when(associateService.updateAssocaiate(any(AssociateDTO.class))).thenReturn(associateDTO);
		
		mockMvc.perform(put("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate)))
		        .andExpect(status().isOk());
		
		Mockito.verify(associateService).updateAssocaiate(any(AssociateDTO.class));
	}
	
	@Test
	void updateAssociateWithException() throws JsonProcessingException, Exception {
		
		Mockito.when(associateService.updateAssocaiate(any(AssociateDTO.class))).thenThrow(AssociateException.class);
		
		mockMvc.perform(put("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associate)))
		        .andExpect(status().isBadRequest());
		
		Mockito.verify(associateService).updateAssocaiate(any(AssociateDTO.class));
	}
	
	@Test
	void testFetchbyGender() throws Exception {
		List<AssociateDTO> associatesDTOs = Arrays.asList(associateDTO);
		Mockito.when(associateService.getAssociatesByGender("M")).thenReturn(associatesDTOs);
		mockMvc.perform(get("/rd/associates/{gender}","M")).andExpect(status().isOk());
		Mockito.verify(associateService).getAssociatesByGender("M");
	}
	
	@BeforeEach
	void setUpAssociate() {
		associate = new Associate();
		associate.setCollege("GPREC");
		associate.setEmail("midhun@epam.com");
		associate.setGender("M");
		associate.setId(1);
		associate.setStaus("ACTIVE");
		associate.setName("Midhun");
		associate.setBatch(batch);
		
		associateDTO = AssociateDTO.builder().batch(batchDTO).college("GPREC")
				                          .email("midhun@epam.com")
				                          .gender("M")
				                          .id(1)
				                          .staus("ACTIVE")
				                          .name("Midhun").build();
	}
	
	@BeforeEach
	void setUpBatch() {
		batch = new Batch();
		batch.setAssociates(Arrays.asList(associate));
		batch.setEndDate(new Date(10));
		batch.setStartDate(new Date(9));
		batch.setPractice("Java");
		batch.setName("Raj");
		
		batchDTO = BatchDTO.builder().endDate(new Date(10))
				           .startDate(new Date(9))
				           .practice("Java")
				           .name("Raj")
				           .id(1)
				           .build();
		
	}

}
